# This file is part of the SV-Benchmarks collection of verification tasks:
# https://github.com/sosy-lab/sv-benchmarks
#
# SPDX-FileCopyrightText: 2023 Jérôme Boillot <jerome.boillot@ens.fr>
#
# SPDX-License-Identifier: Apache-2.0

Verification tasks that require computation through wrap-arounds with relational properties and non-determinism.

Those examples come from:
"Symbolic transformation of expressions in modular arithmetic" (SAS 2023)
https://doi.org/10.1007/978-3-031-44245-2_6
