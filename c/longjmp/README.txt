# This file is part of the SV-Benchmarks collection of verification tasks:
# https://gitlab.com/sosy-lab/benchmarking/sv-benchmarks
#
# SPDX-FileCopyrightText: 2022-2023 University of Tartu & Technische Universität München
#
# SPDX-License-Identifier: MIT

Contributed by:  The Goblint developers

Subset of Goblint regression tests for setjmp/longjmp

Goblint website: https://goblint.in.tum.de/
Goblint repository: https://github.com/goblint/analyzer

The set of regression tests for setjmp/longmp from the `tests/regression/` directory in the repository has been converted using `scripts/regression2sv-benchmarks.py`.